// Preloader
$(window).on("load", function() {
  // Preloader
  $(".loader_inner").fadeOut("slow");
  $(".loader")
    .delay(200)
    .fadeOut("slow");
});

// Main min-height
(function() {
  const main = $(".main");
  const footerHeight = $(".footer").outerHeight();
  const mainHeight = `calc(100vh - ${footerHeight}px)`;
  main.css({
    "min-height": mainHeight
  });
})();

//Smooth anchor scrolling
$(".anchor").click(function(e) {
  e.preventDefault();

  $("html, body").animate(
    {
      scrollTop: $($.attr(this, "href")).offset().top
    },
    500
  );
  return false;
});

//OWL CAROUSEL
$(".check-list").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  items: 3,
  animateIn: "linear",
  animateOut: "linear",
  mouseDrag: false
});
$(document).ready(() => {
  if ($(".owl-stage").length) {
    const owlItems = $(".owl-stage").find(".owl-item");
    const activeSlide = $(".owl-stage").find(".active");
    activeSlide[0].classList.add("active-slide");

    for (let item of owlItems) {
      if (item.classList.contains("active")) {
        item.classList.add("opacity-1");
      } else {
        item.classList.remove("opacity-1");
      }
    }
  }
});
$(".owl-next").click(function() {
  const activeSlide = $(".owl-stage").find(".active");
  const owlItems = $(".owl-stage").find(".owl-item");
  for (let item of owlItems) {
    if (item.classList.contains("active-slide")) {
      item.classList.remove("active-slide");
    }
  }
  for (let item of owlItems) {
    if (item.classList.contains("active")) {
      item.classList.add("opacity-1");
    } else {
      item.classList.remove("opacity-1");
    }
  }
  activeSlide[0].classList.add("active-slide");
});

$(".hat-list").owlCarousel({
  loop: true,
  nav: true,
  items: 1,
  dots: true,
  responsive: {
    0: {
      nav: false,
      dots: true
    },
    992: {
      nav: true,
      dots: true
    }
  }
});

$(".check-list-mobile ").owlCarousel({
  loop: true,
  margin: 10,
  items: 3,
  animateIn: "linear",
  animateOut: "linear"
});

$(".hat-review").owlCarousel({
  loop: true,
  items: 1
});
$(".targeting-review").owlCarousel({
  loop: true,
  items: 1
});

//Mobile menu
$(document).ready(() => {
  const button = $(".burger-button");
  const content = $(".mobile-header_content");
  const wrapper = $(".mobile-header");

  button.click(() => {
    button.hasClass("burger-button-active")
      ? button.removeClass("burger-button-active") &&
        content.removeClass("is-open") &&
        wrapper.removeClass("is-open")
      : button.addClass("burger-button-active") &&
        content.addClass("is-open") &&
        wrapper.addClass("is-open");
  });
});
//Close menu when item selected
$(".mobile-header_item").click(() => {
  const button = $(".burger-button");
  const content = $(".mobile-header_content");
  const wrapper = $(".mobile-header");
  button.hasClass("burger-button-active")
    ? button.removeClass("burger-button-active") &&
      content.removeClass("is-open") &&
      wrapper.removeClass("is-open")
    : button.addClass("burger-button-active") &&
      content.addClass("is-open") &&
      wrapper.addClass("is-open");
});
//FixedHeader
$(window).scroll(function() {
  var sticky = $(".sticky"),
    scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass("fixed");
  else sticky.removeClass("fixed");
});

//Popups
$(".action-button,.outline-button").click(e => {
  let self = $(e.target);
  if (!$(e.target).is(".action-button,.outline-button")) {
    self = self.closest(".action-button,.outline-button");
  }
  const id = self.data("popup");
  const popup = $(`#${id}`);
  const overlay = $(".overlay");
  const body = $("body");

  if (id === "mainForm") {
    const option = self.data("option");
    if (option) {
      const services = $("#services");
      const priceInput = $("#servicePrice");
      const value = services.find(`#${option}`).data("value");
      services
        .find(`#${option}`)
        .prop("selected", true)
        .siblings()
        .prop("selected", false);
      services.trigger("refresh");
      priceInput.val(value);
    }
  }

  popup.addClass("popup-open");
  overlay.addClass("popup-open");
  body.addClass("overflow-hidden");
});
$(".close-cross").click(e => {
  const popup = $(".popup");
  const overlay = $(".overlay");
  const body = $("body");
  popup.removeClass("popup-open");
  overlay.removeClass("popup-open");
  body.removeClass("overflow-hidden");
});
$(".overlay").mouseup(e => {
  const popup = $(".popup");
  const overlay = $(".overlay");
  const body = $("body");
  if (!$(e.target).is(popup) && popup.has(e.target).length === 0) {
    popup.removeClass("popup-open");
    overlay.removeClass("popup-open");
    body.removeClass("overflow-hidden");
  }
});

// Services select //
// Set initial value
(function() {
  const services = $("#services");
  const priceInput = $("#servicePrice");
  const value = services
    .find("option")
    .eq(0)
    .data("value");
  priceInput.val(value);
})();
// Set styling and controls
$("#services").styler();

$("#services").change(function() {
  const priceInput = $("#servicePrice");
  const selected = $(this).find("option:selected");
  const value = selected.data("value");
  priceInput.val(value);
})

$(".reverse__form").submit(function(e) {
    e.preventDefault();
    var data = {};
    var $this = function(selector) {
        return this.find(selector);
    }.bind($(this));

    data.name = $this('input[name="name"]').val();
    data.text = $this('input[name="text"]').val();
    $.ajax({
      type: "POST",
      url: "",
      data: {
        ajax_handler: "contact_form",
        data: data
      },
      dataType: "html",
      beforeSend: function() {},
      complete: function() {},
      error: function() {
          console.log('error');
      },
      success: function(data) {
          $(".reverse__form")[0].reset();
      }
    });
});

$(".index__form").submit(function(e) {
    var data = {};
    var $this = function(selector) {
        return this.find(selector);
    }.bind($(this));

    data.id_order = $this('input[name="ik_pm_no"]').val();
    data.name = $this('input[name="form_name"]').val();
    data.contacts = $this('input[name="form_contacts"]').val();
    data.selected = $this('div[class="jq-selectbox__select-text"]').text();
    data.price = $this('input[name="ik_am"]').val();

    $.ajax({
      type: "POST",
      url: "",
      data: {
        ajax_handler: "interkassa_form",
        data: data
      },
      dataType: "html",
      beforeSend: function() {},
      complete: function() {},
      error: function() {
          console.log('error');
      },
      success: function(data) {
          console.log('submit');
      }
    });
});
