const gulp = require("gulp");

// For styles.
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");

// For include parts of files.
const rigger = require("gulp-rigger");

// For images.
const cleanCSS = require("gulp-clean-css");

// For errors.
const notify = require("gulp-notify");

// For view.
const del = require("del");
const browserSync = require("browser-sync").create();

const root = `./app`;

const config = {
  scss: {
    src: `${root}/scss/**/*.scss`,
    dist: `${root}/css`
  },
  html: {
    src: `${root}/dev_html/*.html`,
    dist: `${root}/html`
  },
  js: `${root}/js/**/*.js`
};

// Server.
const serverConfig = {
  server: {
    baseDir: root,
    directory: true
  },
  startPath: "html/index.html",
  notify: false
};

gulp.task("browser-sync", function() {
  browserSync.init(serverConfig);
  browserSync.watch([`${root}/**/*.*`], browserSync.reload);
});

gulp.task("html", function() {
  return gulp
    .src(config.html.src)
    .pipe(rigger()) //позволяет использовать такую конструкцию для импорта файлов:  //= template/footer.html
    .pipe(gulp.dest(config.html.dist));
});

gulp.task("scss", function() {
  return (
    gulp
      .src(config.scss.src)
      // .pipe(sourcemaps.init())
      .pipe(sass({ outputStyle: "expanded" }).on("error", notify.onError()))
      .pipe(autoprefixer(["last 15 versions"]))
      .pipe(cleanCSS()) // Опционально, закомментировать при отладке
      // .pipe(sourcemaps.write('/'))
      .pipe(gulp.dest(config.scss.dist))
  );
});

gulp.task("clean", function() {
  return del([config.html.dist, config.scss.dist], { force: true });
});

gulp.task("watch", function() {
  gulp.watch(config.html.src, gulp.series("html"));
  gulp.watch(config.scss.src, gulp.series("scss"));
  gulp.watch(config.js);
});

gulp.task(
  "default",
  gulp.series(
    ["clean"],
    gulp.parallel(["html"], ["scss"]),
    gulp.parallel(["watch"], ["browser-sync"])
  )
);
